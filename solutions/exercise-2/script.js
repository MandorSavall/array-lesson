/*
Напишите код, Который работает так: если в массиве englishBreakfeast больше 4х наименований, перенесите лишнее остальные на обед (массив englishDinner).
Выведите оба массива в консоль
*/
const englishBreakfeast = ["бекон", "яйца", "жареные помидоры", "грибы", "фасоль", "колбаса", "тосты с джемом"];
const englishDinner = [];
while (englishBreakfeast.length > 3) {
    const food = englishBreakfeast.pop();
    englishDinner.push(food);
}
console.log(englishBreakfeast);
console.log(englishDinner);
