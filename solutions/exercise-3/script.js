/*
 Перед вами массив с породами собак.
 Напишите код, который работает так:
 1. Если в массиве есть "ретривер" - замените его на "золотистый ретвивер".
 2. Если в массиве нет "корги"  выведите в консоль фразу "летающая сарделька не найдена!".
 */

const dogs = ["немецкая овчарка", "лабрадор", "ретривер", "хаски", "самоед"];

let index = dogs.indexOf("ретривер");
if (index !== -1) {
    dogs[index] = "золотистый ретвивер";
}
console.log(dogs);
if (!dogs.includes("корги")) {
    console.log("летающая сарделька не найдена!");
}
