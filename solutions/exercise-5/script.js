/*
Напишите код, который работает так: 
1. Заменяет в массиве englishBreakfeast "бекон" на "сосиски".
2. Удаляет из массива englishBreakfeast "колбаса".
3. Вырезает из массива englishBreakfeast все элементы после 3-го и до конца, и сохраняет их в массив englishDinner.
Выведите оба массива в консоль после всех действий.
*/
const englishBreakfeast = ["бекон", "яйца", "жареные помидоры", "грибы", "фасоль", "колбаса", "тосты с джемом"];
const index1 = englishBreakfeast.indexOf("бекон");
englishBreakfeast.splice(index1, 1, "сосиски");
const index2 = englishBreakfeast.indexOf("колбаса");
englishBreakfeast.splice(index2, 1);
englishDinner = englishBreakfeast.splice(3);
console.log(englishBreakfeast);
console.log(englishDinner);