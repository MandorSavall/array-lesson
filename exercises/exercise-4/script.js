/*
Перед вами массив (characters) персонажей "Гарри Поттер и методы рационального мышления". Напишите код, который работает так:
1. Создает новый массив students, в который входят все элементы исходного массива от начала и до "профессор Квиррелл" (не включая его).
2. Создает новый массив teachers, который состоит из элементов исходного массива, начинающихся с "профессор Квиррелл", и заканчивающихся "Северус Снейп".
3. Создает новый массив otherPeople, состояющий из всех элементов массива после "Северус Снейп"
*/
